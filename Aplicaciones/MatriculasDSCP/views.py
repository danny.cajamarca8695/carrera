from django.shortcuts import render, redirect
from .models import Carrera,Curso, Asignatura
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.db.models.deletion import ProtectedError
# Create your views here.
def plantilla(request):
    return render(request,'cuerpo.html')
def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario])

        messages.success(request, 'Se ha enviado tu correo')
        return HttpResponseRedirect('/')  # Puedes redirigir a una página de éxito

    return render(request, 'cuerpo.html')


def carrera(request):
    carreras = Carrera.objects.all()
    return render(request, 'carrera.html', {'carreras': carreras})
def crearCarrera(request):
    nombreCarreradcp = request.POST.get("nombre")
    directorCarreradcp = request.POST.get("director")
    duracion_aniosCarreradcp = request.POST.get("duracion_anios")
    logo = request.FILES.get("logo")
    nuevoCarrera=Carrera.objects.create(
    nombreCarreradcp= nombreCarreradcp,
    directorCarreradcp= directorCarreradcp,
    logoCarreradcp=logo,
    duracion_aniosCarreradcp= duracion_aniosCarreradcp,
    )
    messages.success(request, 'Carrera guardado exitosamente')
    return redirect('/carrera/')
def eliminarCarrera(request, id):
    try:
        carreraEliminar = Carrera.objects.get(idCarreradcp=id)
        carreraEliminar.delete()
        messages.success(request, 'Carrera eliminada exitosamente')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar la carrera porque tiene elementos asociados')
    return redirect('/carrera/')
def editarCarrera(request, id):
    carreraEditar = Carrera.objects.get(idCarreradcp=id)
    return render(request, 'editarCarrera.html', {'carreraEditar': carreraEditar})

def actualizarCarrera(request):
    id = request.POST.get("id")
    nombreCarreradcp = request.POST.get("nombre")
    directorCarreradcp = request.POST.get("director")
    duracion_aniosCarreradcp = request.POST.get("duracion_anios")
    fotografia = request.FILES.get("logo")
    carreraEditar = get_object_or_404(Carrera, idCarreradcp=id)

    # Verificar si se proporcionó una nueva fotografía
    if fotografia:
        carreraEditar.logoCarreradcp = fotografia

    carreraEditar.nombreCarreradcp= nombreCarreradcp
    carreraEditar.directorCarreradcp= directorCarreradcp
    carreraEditar.duracion_aniosCarreradcp= duracion_aniosCarreradcp
    carreraEditar.save()

    messages.success(request, 'Carrera Actualizada Exitosamente')
    return redirect('/carrera/')


def curso(request):
    cursos = Curso.objects.all()
    carreras = Carrera.objects.all()
    return render(request, 'curso.html', {'cursos': cursos,'carreras': carreras})
def crearCurso(request):
    nivelCursodcp = request.POST.get("nivel")
    descripcionCursodcp = request.POST.get("descripcion")
    aulaCursodcp = request.POST.get("aula")
    horarioCursodcp = request.POST.get("horario")
    tutorCursodcp = request.POST.get("tutor")
    carrera= request.POST["carrera"]
    # trafomo a onjeto
    carreraSeleccionado=Carrera.objects.get(idCarreradcp=carrera)


    nuevoCurso=Curso.objects.create(
    nivelCursodcp= nivelCursodcp,
    descripcionCursodcp= descripcionCursodcp,
    aulaCursodcp=aulaCursodcp,
    horarioCursodcp= horarioCursodcp,
    tutorCursodcp=tutorCursodcp,
    carrera= carreraSeleccionado
    )
    messages.success(request, 'Curso guardado exitosamente')
    return redirect('/curso/')
def eliminarCurso(request, id):
    try:
        cursoEliminar = Curso.objects.get(idCursodcp=id)
        cursoEliminar.delete()
        messages.success(request, 'Curso eliminado exitosamente')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar el curso porque tiene elementos asociados')
    return redirect('/curso/')
def editarCurso(request, id):
    cursoEditar = Curso.objects.get(idCursodcp=id)
    carreras = Carrera.objects.all()
    return render(request, 'editarCurso.html', {'curso': cursoEditar,'carreras': carreras})

def actualizarCurso(request):
    id = request.POST.get("id")
    nivelCursodcp = request.POST.get("nivel")
    descripcionCursodcp = request.POST.get("descripcion")
    aulaCursodcp = request.POST.get("aula")
    horarioCursodcp = request.POST.get("horario")
    tutorCursodcp = request.POST.get("tutor")
    carrera= request.POST["carrera"]
    # trafomo a onjeto
    carreraSeleccionado=Carrera.objects.get(idCarreradcp=carrera)

    cursoEditar = get_object_or_404(Curso, idCursodcp=id)
    cursoEditar.nivelCursodcp= nivelCursodcp
    cursoEditar.descripcionCursodcp= descripcionCursodcp
    cursoEditar.aulaCursodcp= aulaCursodcp
    cursoEditar.horarioCursodcp= horarioCursodcp
    cursoEditar.tutorCursodcp= tutorCursodcp
    cursoEditar.carrera= carreraSeleccionado
    cursoEditar.save()

    messages.success(request, 'Curso Actualizada Exitosamente')
    return redirect('/curso/')

# Asignatura

def asignatura(request):
    asignaturas = Asignatura.objects.all()
    cursos = Curso.objects.all()
    return render(request, 'asignatura.html', {'asignaturas': asignaturas,'cursos': cursos})
def crearAsignatura(request):
    nombreAsignaturadcp = request.POST.get("nombre")
    creditosAsignaturadcp = request.POST.get("creditos")
    fecha_inicioAsignaturadcp = request.POST.get("fechaInicio")
    fecha_finalAsignaturadcp = request.POST.get("fechaFinal")
    profesorAsignaturadcp = request.POST.get("profesor")
    semestreAsignaturadcp = request.POST.get("semestre")
    descripcionAsignaturadcp = request.POST.get("descripcion")
    silaboAsignaturadcp = request.FILES.get("silabo")
    curso= request.POST["curso"]
    # trafomo a onjeto
    cursoSeleccionado=Curso.objects.get(idCursodcp=curso)


    nuevaAsignatura=Asignatura.objects.create(
    nombreAsignaturadcp= nombreAsignaturadcp,
    creditosAsignaturadcp= creditosAsignaturadcp,
    fecha_inicioAsignaturadcp=fecha_inicioAsignaturadcp,
    fecha_finalAsignaturadcp= fecha_finalAsignaturadcp,
    profesorAsignaturadcp=profesorAsignaturadcp,
    silaboAsignaturadcp= silaboAsignaturadcp,
    semestreAsignaturadcp=semestreAsignaturadcp,
    descripcionAsignaturadcp=descripcionAsignaturadcp,
    curso= cursoSeleccionado
    )
    messages.success(request, 'Asignatura guardado exitosamente')
    return redirect('/asignatura/')
def eliminarAsignatura(request,id):
    asignaturaEliminar = Asignatura.objects.get(idAsignaturadcp=id)
    asignaturaEliminar.delete()
    messages.success(request, 'asignatura eliminado exitosamente')
    return redirect('/asignatura/')
def editarAsignatura(request, id):
    asignaturaEditar = Asignatura.objects.get(idAsignaturadcp=id)
    cursos = Curso.objects.all()
    return render(request, 'editarAsignatura.html', {'asignatura': asignaturaEditar,'cursos': cursos})


def actualizarAsignatura(request):
    id = request.POST.get("id")
    nombreAsignaturadcp = request.POST.get("nombre")
    creditosAsignaturadcp = request.POST.get("creditos")
    fecha_inicioAsignaturadcp = request.POST.get("fechaInicio")
    fecha_finalAsignaturadcp = request.POST.get("fechaFinal")
    profesorAsignaturadcp = request.POST.get("profesor")
    semestreAsignaturadcp = request.POST.get("semestre")
    descripcionAsignaturadcp = request.POST.get("descripcion")
    silaboAsignaturadcp = request.FILES.get("silabo")
    curso= request.POST["curso"]
        # trafomo a onjeto
    cursoSeleccionado=Curso.objects.get(idCursodcp=curso)

    asignaturaEditar = get_object_or_404(Asignatura, idAsignaturadcp=id)
    if silaboAsignaturadcp:
        asignaturaEditar.silaboAsignaturadcp = silaboAsignaturadcp
    asignaturaEditar.nombreAsignaturadcp= nombreAsignaturadcp
    asignaturaEditar.creditosAsignaturadcp= creditosAsignaturadcp
    asignaturaEditar.fecha_inicioAsignaturadcp= fecha_inicioAsignaturadcp
    asignaturaEditar.fecha_finalAsignaturadcp= fecha_finalAsignaturadcp
    asignaturaEditar.profesorAsignaturadcp= profesorAsignaturadcp
    asignaturaEditar.semestreAsignaturadcp= semestreAsignaturadcp
    asignaturaEditar.descripcionAsignaturadcp= descripcionAsignaturadcp
    asignaturaEditar.curso= cursoSeleccionado
    asignaturaEditar.save()

    messages.success(request, 'Asignatura Actualizada Exitosamente')
    return redirect('/asignatura/')
