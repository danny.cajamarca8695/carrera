
from django.urls import path,include
from . import views
urlpatterns = [
    path('', views.plantilla),
    path('enviar-correo/',views.enviar_correo, name='enviar_correo'),
    path('carrera/', views.carrera),
    path('crearCarrera/', views.crearCarrera, name='crearCarrera'),
    path('eliminarCarrera/<id>',views.eliminarCarrera, name='eliminarCarrera'),
    path('editarCarrera/<id>',views.editarCarrera, name='editarCarrera'),
    path('actualizarCarrera/',views.actualizarCarrera, name='actualizarCarrera'),


    path('curso/', views.curso,name='curso'),
    path('crearCurso/', views.crearCurso, name='crearCurso'),
    path('eliminarCurso/<id>',views.eliminarCurso, name='eliminarCurso'),
    path('editarCurso/<id>',views.editarCurso, name='editarCurso'),
    path('actualizarCurso/',views.actualizarCurso, name='actualizarCurso'),

    path('asignatura/', views.asignatura,name='asignatura'),
    path('crearAsignatura/', views.crearAsignatura, name='crearAsignatura'),
    path('eliminarAsignatura/<id>',views.eliminarAsignatura, name='eliminarAsignatura'),
    path('editarAsignatura/<id>',views.editarAsignatura, name='editarAsignatura'),
    path('actualizarAsignatura/',views.actualizarAsignatura, name='actualizarAsignatura'),

]
