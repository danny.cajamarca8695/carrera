from django.db import models

class Carrera(models.Model):
    idCarreradcp = models.AutoField(primary_key=True)
    nombreCarreradcp = models.CharField(max_length=255)
    directorCarreradcp = models.CharField(max_length=255)
    logoCarreradcp =models.FileField(upload_to='logo',null=True,blank=True)
    duracion_aniosCarreradcp = models.PositiveIntegerField()
    fecha_creacionCarreradcp = models.DateField(auto_now_add=True)
    def __str__(self):
        return self.nombreCarreradcp
class Curso(models.Model):
    idCursodcp = models.AutoField(primary_key=True)
    nivelCursodcp = models.CharField(max_length=100)
    descripcionCursodcp = models.TextField()
    aulaCursodcp = models.CharField(max_length=50)
    horarioCursodcp = models.CharField(max_length=50, blank=True, null=True)
    tutorCursodcp = models.CharField(max_length=100, blank=True, null=True)
    carrera = models.ForeignKey(Carrera,null=True,blank=True,on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.nivelCursodcp} - {self.descripcionCursodcp}"



class Asignatura(models.Model):
    idAsignaturadcp = models.AutoField(primary_key=True)
    nombreAsignaturadcp = models.CharField(max_length=100)
    creditosAsignaturadcp = models.IntegerField()
    fecha_inicioAsignaturadcp = models.DateField()
    fecha_finalAsignaturadcp = models.DateField()
    profesorAsignaturadcp = models.CharField(max_length=100)
    silaboAsignaturadcp = models.FileField(upload_to='silabo',null=True,blank=True)
    semestreAsignaturadcp = models.CharField(max_length=10, blank=True, null=True)
    descripcionAsignaturadcp = models.TextField(blank=True, null=True)
    curso = models.ForeignKey(Curso,null=True,blank=True,on_delete=models.PROTECT)


    def __str__(self):
        return self.nombreAsignaturadcp
