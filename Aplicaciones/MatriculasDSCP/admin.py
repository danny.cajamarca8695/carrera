from django.contrib import admin
from .models import Carrera, Curso, Asignatura

admin.site.register(Carrera)
admin.site.register(Curso)
admin.site.register(Asignatura)
