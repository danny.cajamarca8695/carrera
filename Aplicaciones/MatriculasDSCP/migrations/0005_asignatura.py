# Generated by Django 4.2.7 on 2024-01-30 02:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('MatriculasDSCP', '0004_curso'),
    ]

    operations = [
        migrations.CreateModel(
            name='Asignatura',
            fields=[
                ('idAsignaturadcp', models.AutoField(primary_key=True, serialize=False)),
                ('nombreAsignaturadcp', models.CharField(max_length=100)),
                ('creditosAsignaturadcp', models.IntegerField()),
                ('fecha_inicioAsignaturadcp', models.DateField()),
                ('fecha_finalAsignaturadcp', models.DateField()),
                ('profesorAsignaturadcp', models.CharField(max_length=100)),
                ('silaboAsignaturadcp', models.FileField(blank=True, null=True, upload_to='silabo')),
                ('semestreAsignaturadcp', models.CharField(blank=True, max_length=10, null=True)),
                ('descripcionAsignaturadcp', models.TextField(blank=True, null=True)),
                ('curso', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='MatriculasDSCP.curso')),
            ],
        ),
    ]
